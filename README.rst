libskaro for skaro protocol
===========================

|build status|

Skaro is an open standard for instant messaging. `More about
skaro <https://bitbucket.org/exarh-team/skarim-server/wiki/Home>`__.

libskaro - a library that simplifies working with skaro protocol.

Documentation
-------------

See the documentation for `installation and usage instructions`_.

.. _installation and usage instructions: https://libskaro.readthedocs.io/

Third-party software
--------------------

libskaro includes the following third-party software:

-  Simple event observer system (libskaro/event.py) by Tom Dryer is
   licensed under the MIT License: https://github.com/tdryer/hangups/

.. |build status| image:: https://gitlab.com/exarh-team/libskaro/badges/master/build.svg
   :target: https://gitlab.com/exarh-team/libskaro/commits/master
