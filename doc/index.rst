.. libskaro documentation master file, created by
   sphinx-quickstart on Sat May  7 23:27:29 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to libskaro's documentation!
====================================

Quick Links
-----------

* `libskaro on GitLab`_
* `Skaro-QML on GitLab`_
* `SkarIM-Server on BitBucket`_

  .. _libskaro on GitLab: https://gitlab.com/exarh-team/libskaro
  .. _Skaro-QML on GitLab: https://gitlab.com/exarh-team/skaro-qml
  .. _SkarIM-Server on BitBucket: https://bitbucket.org/exarh-team/skarim-server


Documentation Contents
----------------------

.. toctree::
   :maxdepth: 2

   installation
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
