API
===

Public methods
--------------

.. automodule:: libskaro.client

    .. autoclass:: Client

        .. automethod:: __init__

        .. rubric:: Registration / Authorization

        .. automethod:: connect
        .. automethod:: authorize
        .. automethod:: register_type
        .. automethod:: session_restore

        .. rubric:: Work with contacts

        .. automethod:: get_contacts

        .. rubric:: Work with messages

        .. automethod:: get_history
        .. automethod:: send_message


Private methods
---------------

        .. rubric:: Registration / Authorization

        .. automethod:: Client._check_connection
        .. automethod:: Client._authorize_on_request
        .. automethod:: Client._register_type_on_request
        .. automethod:: Client._session_restore_on_request

        .. rubric:: Work with contacts

        .. automethod:: Client._get_contacts_on_request

        .. rubric:: Work with messages

        .. automethod:: Client._get_history_on_request
        .. automethod:: Client._send_message_on_request
        .. automethod:: Client._show_message

        .. rubric:: Other

        .. automethod:: Client._info
        .. automethod:: Client._error
        .. automethod:: Client._received_message
        .. automethod:: Client._init_user
        .. automethod:: Client.__write_client__